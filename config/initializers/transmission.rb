require 'transmission'

Transmission::Config.set host: 'localhost', port: 9091, ssl: false, credentials: {username: 'hibiki', password: ENV['TRANSMISSION_PASS']}