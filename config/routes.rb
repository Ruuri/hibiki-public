Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :tracks do
    member do
      post "like"
      post "unlike"
      post "listen"
    end
    collection do
      get 'new_tracks'
      get 'last_listened_to'
    end
  end
  resources :albums do
    member do 
      get "download"
    end
  end
  resources :artists do

  end

  resources :mine do

  end
  resources :playlists do
    member do
      post "add_track"
    end
  end
  
  devise_for :users, controllers: { sessions: 'sessions' }
  get "/users/me", to: "mine#index"

  resources :users do
    member do
      get "likes"
    end
  end

  resources :searches do
    collection do
      get "search"
    end
  end

  get "/search" => "searches#search"
end
