class CreateContributors < ActiveRecord::Migration[5.0]
  def change
    create_table :contributors do |t|
      t.references :artist, foreign_key: true
      t.references :track, foreign_key: true
      t.string :role

      t.timestamps
    end
  end
end
