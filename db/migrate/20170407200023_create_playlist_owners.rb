class CreatePlaylistOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :playlist_owners do |t|
      t.references :user, foreign_key: true
      t.references :playlist, foreign_key: true

      t.timestamps
    end
  end
end
