class AddAttachmentSourceToTracks < ActiveRecord::Migration
  def self.up
    change_table :tracks do |t|
      t.attachment :source
    end
  end

  def self.down
    remove_attachment :tracks, :source
  end
end
