class CreateSources < ActiveRecord::Migration[5.0]
  def change
    create_table :sources do |t|
      t.text :url
      t.text :title

      t.timestamps
    end
  end
end
