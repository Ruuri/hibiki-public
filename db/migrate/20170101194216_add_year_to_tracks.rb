class AddYearToTracks < ActiveRecord::Migration[5.0]
  def change
    add_column :tracks, :year, :string
  end
end
