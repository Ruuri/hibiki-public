class AddRawLyricsToTracks < ActiveRecord::Migration[5.0]
  def change
    add_column :tracks, :raw_lyrics, :text
  end
end
