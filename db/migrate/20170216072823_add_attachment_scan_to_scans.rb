class AddAttachmentScanToScans < ActiveRecord::Migration
  def self.up
    change_table :scans do |t|
      t.attachment :scan
    end
  end

  def self.down
    remove_attachment :scans, :scan
  end
end
