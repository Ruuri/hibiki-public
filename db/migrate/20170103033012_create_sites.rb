class CreateSites < ActiveRecord::Migration[5.0]
  def change
    create_table :sites do |t|
      t.text :last_torrent_name

      t.timestamps
    end
  end
end
