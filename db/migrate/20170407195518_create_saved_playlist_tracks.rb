class CreateSavedPlaylistTracks < ActiveRecord::Migration[5.0]
  def change
    create_table :saved_playlist_tracks do |t|
      t.references :playlist, foreign_key: true
      t.references :user, foreign_key: true
      t.references :track, foreign_key: true

      t.timestamps
    end
  end
end
