# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170407200023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "album_art_file_name"
    t.string   "album_art_content_type"
    t.integer  "album_art_file_size"
    t.datetime "album_art_updated_at"
    t.string   "year"
  end

  create_table "artists", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  create_table "contributors", force: :cascade do |t|
    t.integer  "artist_id"
    t.integer  "track_id"
    t.string   "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["artist_id"], name: "index_contributors_on_artist_id", using: :btree
    t.index ["track_id"], name: "index_contributors_on_track_id", using: :btree
  end

  create_table "likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "track_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["track_id"], name: "index_likes_on_track_id", using: :btree
    t.index ["user_id"], name: "index_likes_on_user_id", using: :btree
  end

  create_table "playlist_owners", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "playlist_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["playlist_id"], name: "index_playlist_owners_on_playlist_id", using: :btree
    t.index ["user_id"], name: "index_playlist_owners_on_user_id", using: :btree
  end

  create_table "playlists", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "saved_playlist_tracks", force: :cascade do |t|
    t.integer  "playlist_id"
    t.integer  "user_id"
    t.integer  "track_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["playlist_id"], name: "index_saved_playlist_tracks_on_playlist_id", using: :btree
    t.index ["track_id"], name: "index_saved_playlist_tracks_on_track_id", using: :btree
    t.index ["user_id"], name: "index_saved_playlist_tracks_on_user_id", using: :btree
  end

  create_table "scans", force: :cascade do |t|
    t.integer  "album_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "scan_file_name"
    t.string   "scan_content_type"
    t.integer  "scan_file_size"
    t.datetime "scan_updated_at"
    t.index ["album_id"], name: "index_scans_on_album_id", using: :btree
  end

  create_table "sites", force: :cascade do |t|
    t.text     "last_torrent_name"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "sources", force: :cascade do |t|
    t.text     "url"
    t.text     "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tracks", force: :cascade do |t|
    t.string   "title"
    t.integer  "album_id"
    t.integer  "order"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "source_file_name"
    t.string   "source_content_type"
    t.integer  "source_file_size"
    t.datetime "source_updated_at"
    t.text     "metadata"
    t.string   "year"
    t.integer  "length"
    t.text     "raw_lyrics"
    t.index ["album_id"], name: "index_tracks_on_album_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "authentication_token"
    t.string   "username"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "contributors", "artists"
  add_foreign_key "contributors", "tracks"
  add_foreign_key "likes", "tracks"
  add_foreign_key "likes", "users"
  add_foreign_key "playlist_owners", "playlists"
  add_foreign_key "playlist_owners", "users"
  add_foreign_key "saved_playlist_tracks", "playlists"
  add_foreign_key "saved_playlist_tracks", "tracks"
  add_foreign_key "saved_playlist_tracks", "users"
  add_foreign_key "scans", "albums"
  add_foreign_key "tracks", "albums"
end
