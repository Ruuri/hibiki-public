#!/bin/bash

cd /var/www/hibiki/current

source /home/hibiki/.profile
source /home/hibiki/.rvm/environments/ruby-2.2.6

bundle exec rails tracks:acquire_tracks RAILS_ENV=production