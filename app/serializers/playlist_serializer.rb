class PlaylistSerializer < ActiveModel::Serializer
  attributes :id, :name 

  has_many :users
  has_many :tracks
end