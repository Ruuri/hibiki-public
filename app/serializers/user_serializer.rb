class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :mine, :token, :links, :username

  def mine
    scope ? scope.id == object.id : false
  end
  def token
    scope ? @instance_options[:token] : nil
  end
  def links
    {
      likes: "/mine/likes"
    }
  end
end