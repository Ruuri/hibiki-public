class AlbumSerializer < ActiveModel::Serializer
  attributes :id, :title, :album_art, :album_art_thumb, :year

  has_many :tracks
  has_many :artists

  def album_art
    if Rails.env.development?
      "http://localhost:3000#{object.album_art.url(:original)}"
    else
      object.album_art.url(:original)
    end
  end

  def album_art_thumb
    if Rails.env.development?
      "http://localhost:3000#{object.album_art.url(:thumb)}"
    else
      object.album_art.url(:thumb)
    end
  end
end
