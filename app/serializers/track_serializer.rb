class TrackSerializer < ActiveModel::Serializer
  attributes :id, :source, :length, :order, :title, :small_source, :raw_lyrics, :liked, 
             :original_source, :album_art
  belongs_to :album

  has_many :artists

  def source
    if Rails.env.development?
      "http://localhost:3000#{object.source.url(:downsampled)}"
    else
      object.source.url(:downsampled)
    end
  end
  def original_source
    if Rails.env.development?
      "http://localhost:3000#{object.source.url(:original)}"
    else
      object.source.url(:original)
    end
  end

  def liked
    scope ? scope.liked?(object.id) : false
  end

  def album_art
    object.album.album_art.url(:thumb)
  end

  def small_source
    if Rails.env.development?
      "http://localhost:3000#{object.source.url(:downsampled)}"
    else
      object.source.url(:downsampled)
    end
  end
end
