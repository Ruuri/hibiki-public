class PlaylistsController < ApplicationController
  before_action :authenticate_user_from_token!

  def index
    load_playlists
    render_playlists
  end
  def create
    create_playlist
    render_playlist
  end
  def show
    load_playlist
    render_playlist
  end
  def destroy
    load_playlist
    destroy_playlist
  end
  def add_track
    load_playlist
    add_track_to_playlist
    render_playlist
  end

  private
  def load_playlist
    @playlist = Playlist.find(params[:id])
  end
  def load_playlists
    if params[:filter]
      @playlists = Playlist.find(params[:filter][:id].split(","))
    else
      offset = params[:offset] ? params[:offset].to_i : 0
      limit = params[:limit] ? params[:limit].to_i : 25

      @playlists = Playlist.all.order('id DESC').limit(limit).offset(offset)
    end
  end
  def create_playlist
    @playlist = Playlist.create(name: playlist_params[:name])
    if !params[:data][:attributes][:trackholder].blank?
      @playlist.tracks << Track.find(params[:data][:attributes][:trackholder])
    end
  end
  def add_track_to_playlist
    @playlist.tracks << Track.find(params[:track])
  end
  def destroy_playlist

  end
  def render_playlist
    render json: @playlist
  end
  def render_playlists
    render json: @playlists, each_serializer: PlaylistSerializer, root: "playlists"
  end
  def playlist_params
    playlist_params = params[:data][:attributes].permit(:name, :trackholder)
  end
end