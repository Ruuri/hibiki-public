class TracksController < ApplicationController
  before_action :authenticate_user_from_token!
  
  def index 
    load_tracks
    render_tracks
  end
  def show
    load_track
    render_track
  end
  def like
    load_track
    like_track
  end
  def unlike
    load_track
    unlike_track
  end
  def new_tracks
    load_new_tracks
    render_tracks
  end
  def listen
    load_track
    add_track_to_listened_queue
  end
  def last_listened_to
    load_listened_to_tracks
  end

  private
  def load_tracks
    if params[:filter]
      @tracks = Track.find(params[:filter][:id].split(","))
    else
      offset = params[:offset] ? params[:offset].to_i : 0
      limit = params[:limit] ? params[:limit].to_i : 25

      @tracks = Track.all.order('id DESC').limit(limit).offset(offset)
    end
  end
  def load_track
    @track = Track.find(params[:id])
  end
  def like_track
    if current_user.liked_tracks << @track
      @track.reload
      render json: @track, serializer: TrackSerializer, root: "track"
    else
      render :json => { :errors => @track.errors.full_messages }, :status => 422 
    end
  end
  def unlike_track
    if current_user.liked_tracks.destroy(@track)
      @track.reload
      render json: @track, serializer: TrackSerializer, root: "track"
    else
      render :json => { :errors => @track.errors.full_messages }, :status => 422 
    end
  end
  def add_track_to_listened_queue
    listen_queue = Rails.cache.fetch(["__listened-to"]) do
      []
    end
    listen_queue.unshift(@track.id)
    listen_queue = listen_queue.slice(0, 200)
    Rails.cache.write(["__listened-to"], listen_queue)
    render json: {ok: "ok"}
  end
  def load_new_tracks
    offset = params[:offset] ? params[:offset].to_i : 0
    limit = params[:limit] ? params[:limit].to_i : 24
    @tracks = Track.all.order('id DESC').limit(limit)
  end
  def load_listened_to_tracks
    offset = params[:offset] ? params[:offset].to_i : 0
    limit = params[:limit] ? params[:limit].to_i : 12

    listen_queue = Rails.cache.fetch(["__listened-to"]) do
      []
    end

    tracks = listen_queue.slice(offset, limit)

    hydrated_tracks = Track.where(id: tracks)
    @tracks = []


    tracks.each do |id|
      @tracks << hydrated_tracks.where(id: id)
    end
   
    @tracks = @tracks.flatten

    render json: @tracks, each_serializer: TrackSerializer, root: "tracks", meta: {played: tracks}
  end
  def render_track
    render json: @track
  end
  def render_tracks
    render json: @tracks, each_serializer: TrackSerializer, root: "tracks"
  end
end
