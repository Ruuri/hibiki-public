class SearchesController < ApplicationController
  before_action :authenticate_user_from_token!
  def title_search
    @results = Track.where("title ILIKE ?", "%"+params[:query]+"%")
  end

  def artist_search
    @results = Track.joins(:artists).where("name ILIKE ?", "%"+params[:query]+"%").distinct
  end

  def lyrics_search
    @results = Track.where("raw_lyrics ILIKE ?", "%"+params[:query]+"%")
  end

  def all_search
    @results = Track.joins(:artists, :album).where("(tracks.title ILIKE ?) OR (name ILIKE ?) OR (raw_lyrics ILIKE ?) OR (albums.title ILIKE ?)", "%"+params[:query]+"%", "%"+params[:query]+"%", "%"+params[:query]+"%", "%"+params[:query]+"%").references(:albums).distinct
  end

  def search
    case params[:type]
    when "title"
      title_search
    when "artist"
      artist_search
    when "lyrics"
      lyrics_search
    else
      all_search
    end
    render json: @results, each_serializer: TrackSerializer, root: "tracks"
  end

end
