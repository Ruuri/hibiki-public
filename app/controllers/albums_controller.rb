class AlbumsController < ApplicationController
  require 'zip'
  require 'open-uri'

  def index 
    load_albums
    render_albums
  end
  def show
    load_album
    render_album
  end
  def download
    load_album
    stream_album
  end

  private
  def load_albums
    if params[:filter]
      @albums = Album.find(params[:filter][:id].split(","))
    else
      offset = params[:offset] ? params[:offset].to_i : 0
      limit = params[:limit] ? params[:limit].to_i : 25
      
      @albums = Album.all.order('id DESC').limit(limit).offset(offset)
    end
  end
  def load_album
    @album = Album.find(params[:id])
  end
  def render_album
    render json: @album
  end
  def stream_album
    zip_stream = Zip::OutputStream.write_buffer do |zip|
      @album.tracks.each do |track|
        file_obj = open(track.source.url(:original)){ |f| f.read }
        zip.put_next_entry("#{track.title}.mp3")
        zip.print file_obj
      end
    end

    zip_stream.rewind

    send_data zip_stream.read, filename: "#{@album.title}.zip"
  end
  def render_albums
    render json: @albums, each_serializer: AlbumSerializer, root: "albums"
  end
end
