class MineController < ApplicationController
  # include ActionController::HttpAuthentication::Token::ControllerMethods
  # acts_as_token_authentication_handler_for User, fallback: :none
  before_action :authenticate_user_from_token!

  def index

    render json: current_user, serializer: UserSerializer, root: "user"
  end
end