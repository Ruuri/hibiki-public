class ArtistsController < ApplicationController
  def index
    load_artists
    render_artists
  end
  def show
    load_artist
    render_artist
  end

  private
  def load_artist
    @artist = Artist.find(params[:id])
  end
  def load_artists
    if params[:filter]
      @artists = Artist.find(params[:filter][:id].split(","))
    else
      offset = params[:offset] ? params[:offset].to_i : 0
      limit = params[:limit] ? params[:limit].to_i : 25

      @artists = Artist.all.order('id DESC').limit(limit).offset(offset)
    end
  end
  def render_artist
    render json: @artist
  end
  def render_artists
    render json: @artists
  end
end