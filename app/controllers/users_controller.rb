class UsersController < ApplicationController
  before_action :authenticate_user_from_token!, only: [:me, :likes]

  def create
    create_user
    render_user
  end
  def index
    load_users
    render_users
  end
  def show
    find_user
    render_user
  end
  def likes
    find_user
    load_user_likes
    render_tracks
  end
  def me
    render_current_user
  end
  private
  def create_user
    @user = User.create(user_params)
  end
  def find_user
    if params[:id] == "me"
      @user = current_user
    else
      @user = User.find(params[:id])
    end
  end
  def render_user
    render json: @user
  end
  def render_current_user
    
    data = {
      id: current_user.id,
      token: current_user.authentication_token,
      email: current_user.email,
      username: current_user.username,
      mine: true
    }

    render json: data, status: 201
  end
  def load_user_likes
    limit = params[:limit] ? params[:limit].to_i : 12
    offset = params[:offset] ? params[:offset].to_i : 0

    @tracks = @user.liked_tracks.offset(offset).limit(limit).order("likes.id DESC")
  end
  def load_users
    @users = User.all
  end
  def render_users
    render json: @users, each_serializer: UserSerializer, root: "users"
  end
  def render_tracks
    render json: @tracks, each_serializer: TrackSerializer, root: "tracks"
  end
  def user_params
    user_params = params[:data][:attributes].permit(:email, :password)
  end
end