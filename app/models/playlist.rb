class Playlist < ApplicationRecord
  has_many :saved_playlist_tracks, dependent: :destroy
  has_many :tracks, through: :saved_playlist_tracks
  has_many :playlist_owners, dependent: :destroy
  has_many :users, through: :playlist_owners
end
