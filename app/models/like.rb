class Like < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :track, optional: true

  validates :user_id, :uniqueness => { :scope => :track_id }
end
