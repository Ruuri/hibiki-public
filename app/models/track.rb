require 'taglib'
require 'uri'
require 'nokogiri'

class Track < ApplicationRecord
  # serialize :metadata
  belongs_to :album, optional: true

  has_many :contributors
  has_many :artists, through: :contributors
  has_many :likes
  has_many :users, through: :likes

  has_attached_file :source, styles: {downsampled: "198"},
                             processors: [:downsample]

  #do_not_validate_attachment_file_type :source
  validates_attachment_content_type :source,:content_type => [ 'audio/mpeg', 'audio/x-mpeg', 'audio/mp3', 'audio/x-mp3', 'audio/mpeg3', 'audio/x-mpeg3', 'audio/mpg', 'audio/x-mpg', 'audio/x-mpegaudio' ]
  # validates_attachment_content_type :source, content_type:  /\Aaudio\/.*\z/


  before_create :extract_metadata
  after_create :get_lyrics

  def self.display_metadata(track)
    TagLib::MPEG::File.open(track) do |info|
      metadata = info.id3v2_tag
      
      puts metadata.title
      puts metadata.track
      #puts info.audio_properties.length

      puts metadata.album
      puts metadata.year

      # puts metadata.frame_list("APIC").first
      # puts metadata.frame_list("APIC").first.picture
    end
  end
  def extract_metadata
    path = source.queued_for_write[:original].path

    begin
      TagLib::MPEG::File.open(path) do |info|
        metadata = info.id3v2_tag
        self.title = metadata.title
        self.order = metadata.track
        self.year = metadata.year
        self.length = info.audio_properties.length

        album = metadata.album

        album = Album.where(title: album, year: metadata.year).first_or_create
        if metadata.frame_list("APIC").first
          album_art = StringIO.new(Base64.decode64(metadata.frame_list("APIC").first.picture))
          album_art_type = metadata.frame_list("APIC").first.mime_type
          extension = case album_art_type
            when 'image/jpeg', 'image/jpg'
              'jpg'
            when 'image/png'
              'png'
            when 'image/gif'
              'gif'
            else
              raise "Mime not found" 
          end
          
          file_name = "#{album.title}.#{extension}"

          File.open("/tmp/#{file_name}", "wb") do |f|
            f.write(metadata.frame_list("APIC").first.picture)
          end

          album.album_art = File.open("/tmp/#{file_name}", "r")
          album.save
        end
        if album
          self.album = album
        else
          self.album = Album.find_by(title: "Unknown Album")
        end

        artists = metadata.artist.to_s.split(/[、,]+/)
        artists.each do |artist|
          artist = artist.strip
          artist = Artist.where(name: artist).first_or_create

          self.contributors << Contributor.create(artist_id: artist.id, role: "artist")
        end

        composers = metadata.frame_list("TPE2").first.to_s.split(/[、,]+/)
        composers.each do |composer|
          composer = composer.strip
          composer = Artist.where(name: composer).first_or_create

          self.contributors << Contributor.create(artist_id: composer.id, role: "composer")
        end

      end
    rescue Errno::ENOENT => e
      true
    end
  end

  def get_lyrics
    user_agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"
    
    html = "http://sp.uta-net.com/search/index.php?keyword=#{URI.encode(self.title.encode('SHIFT_JIS', 'UTF-8', invalid: :replace, undef: :replace))}&target=song&type=eq"
    # html = "http://sp.uta-net.com/search/index.php?keyword=#{URI.encode('Elemental World')}&target=song&type=eq"
    doc = Nokogiri::HTML(open(html, 'User-Agent' => user_agent))

    items = doc.css("#search_songlist > ul > li > a")

    if items.first
      lyrics_id = items.first.attr('href').gsub("./kashi.php?TID=", "")

      lyrics_url = "http://sp.uta-net.com/search/kashi.php?TID=#{lyrics_id}"

      doc = Nokogiri::HTML(open(lyrics_url, 'User-Agent' => user_agent).read)
      doc.encoding = 'utf-8'
      items = doc.css("#kashi_main")

      self.raw_lyrics = items.inner_html
      self.save
    end
  end

  def get_lyrics_from_id(id)
    user_agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"
    lyrics_url = "http://sp.uta-net.com/search/kashi.php?TID=#{id}"

    doc = Nokogiri::HTML(open(lyrics_url, 'User-Agent' => user_agent).read)
    doc.encoding = 'utf-8'
    items = doc.css("#kashi_main")

    self.raw_lyrics = items.inner_html
    self.save
  end

end
