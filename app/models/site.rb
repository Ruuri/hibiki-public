class Site < ApplicationRecord

  def self.get_music_feed
    @site = Site.first ? Site.first : Site.create 

    #source = "https://www.nyaa.se/?page=rss&cats=3_15&user=321858"  #MASHIN
    source = "http://share.dmhy.org/topics/rss/user_id/677029/rss.xml"
    doc = Nokogiri::XML(open(source))
    items = doc.xpath("/rss/channel/item")   

    if items
      last_item = @site.read_music_helper(items, @site.last_torrent_name)
      @site.last_torrent_name = last_item
      @site.save
    end 
  end

  def read_music_helper(items, last_item)
    if items.first.at('title').text != last_item
      next_last_item = items.first.at('title').text

      items.each do |node|
        raw_title = node.at('title').text
        enclosure = node.at('enclosure').attr('url')

        if raw_title != last_item and !Source.where(url: node.at('enclosure').attr('url')).exists?
          link = node.at('enclosure').attr('url')
          Source.create(title: raw_title, url: link)
          torrent = Transmission::Model::Torrent.add arguments: {filename: link}
        else
          break
        end
      end
      next_last_item
    else
      last_item
    end
  end
end
