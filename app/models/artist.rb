class Artist < ApplicationRecord
  has_many :contributors
  has_many :tracks, through: :contributors
  has_many :albums, through: :tracks
end
