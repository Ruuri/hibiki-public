class SavedPlaylistTrack < ApplicationRecord
  belongs_to :playlist, optional: true
  belongs_to :user, optional: true
  belongs_to :track, optional: true
end
