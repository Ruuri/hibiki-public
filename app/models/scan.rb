class Scan < ApplicationRecord
  belongs_to :album

   has_attached_file :scan, styles: {thumb: "x300>", large: "x1000>"}
end
