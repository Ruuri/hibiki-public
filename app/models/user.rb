class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable

  acts_as_token_authenticatable
  before_save :ensure_authentication_token
  after_create :get_username

  has_many :likes, dependent: :destroy
  has_many :liked_tracks, through: :likes, source: :track

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def liked?(track_id)
    self.liked_tracks.where(id: track_id).exists?
  end
  private
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def get_username
    self.username = self.email.split("@").first
    self.save
  end
  
end
