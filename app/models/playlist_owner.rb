class PlaylistOwner < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :playlist, optional: true
end
