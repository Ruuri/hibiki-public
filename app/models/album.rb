class Album < ApplicationRecord
  has_many :tracks, -> { order(order: :asc)}
  has_many :artists, through: :tracks

  has_many :scans

  has_attached_file :album_art, styles: {thumb: "300x300>", large: "1000x1000>"}, default_url: "https://s3-us-west-2.amazonaws.com/animemusic/assets/missing_album.png"
  validates_attachment_content_type :album_art, content_type:  /\Aimage\/.*\z/
end
