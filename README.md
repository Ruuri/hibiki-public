# README

Important requirements: ffmpeg with libmp3lame, imagemagick, postgres

There are others, but you can let bundle install tell you what you're missing.

Build and install as usual:

1. bundle install (install bundler if it's missing. RVM is a great tool if you need ruby)

2. Modify database.yml.sample to database.yml with your postgres details.

3. rails db:setup

4. rails db:migrate

To populate some tracks, have some mp3s available. It's a little ghetto, but if you load an mp3 file into a new Track model, it'll do everything else automaticaly. For example:

1. open a rails console.

2. f = File.open("/path/to/your.mp3")

3. t = Track.create(source: f)