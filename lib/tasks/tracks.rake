require 'transmission'

namespace :tracks do
  desc "goes through transmissions list of completed files and tries to upload them to the db"
  task collect_tracks: :environment do
    Transmission::Model::Torrent.stop_all!

    Dir.glob('/var/lib/transmission-daemon/downloads/**/*.{mp3,FLAC}') do |file|
      Track.create(source: File.open(file, "r"))
      File.delete(file)
    end

    Dir.glob('/var/lib/transmission-daemon/downloads/**/*.*') do |file|
      if File.file?(file)
        File.delete(file)
      end
    end

    Dir['/var/lib/transmission-daemon/downloads/**/*'].select { |d| File.directory? d }.select { |d| (Dir.entries(d) - %w[ . .. ]).empty? }.each { |d| Dir.rmdir d }

    torrents = Transmission::Model::Torrent.all fields: ['id']
    torrents.ids.each do |id|
      torrent = Transmission::Model::Torrent.find(id)

      if torrent.finished?
        torrent.delete!
      end
    end

    Transmission::Model::Torrent.start_all!
  end

  desc "populate transmission with new music"
  task acquire_tracks: :environment do
    Site.get_music_feed
  end

  desc "get initial set of music"
  task populate_tracks: :environment do

  end
end
