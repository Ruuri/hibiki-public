module Paperclip
  class Downsample < Processor
    
    def initialize(file, options = {}, attachment = nil)
      super
      @file = file
      @attachment = attachment
    
      # set some default parameters for basename, eventual file format
      @basename = File.basename(@file.path, ".mp3")
      @format = options[:format] || "mp3"
      @sample_rate = "44100"
      @bit_rate = "128k"
    end
    def make
      source = @file
      output = Tempfile.new([@basename, ".#{@format}"]) 

      run_string = "-y -i #{File.expand_path(source.path)} -codec:a libmp3lame -qscale:a 5 #{File.expand_path(output.path)}"
      Paperclip.run('ffmpeg', run_string)

      puts "oh yes"
      output
    end
  end
end